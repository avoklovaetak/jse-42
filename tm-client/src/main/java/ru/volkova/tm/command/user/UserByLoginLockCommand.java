package ru.volkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

public class UserByLoginLockCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "lock user by login";
    }

    @Override
    public void execute() {
        if (bootstrap == null) throw new ObjectNotFoundException();
        if (endpointLocator == null) throw new ObjectNotFoundException();
        @Nullable final Session session = bootstrap.getSession();
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        endpointLocator.getAdminUserEndpoint().lockByLogin(session, login);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
