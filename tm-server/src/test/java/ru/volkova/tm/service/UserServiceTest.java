package ru.volkova.tm.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.service.IAdminUserService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.IUserService;
import ru.volkova.tm.dto.UserDTO;
import ru.volkova.tm.marker.UnitCategory;

public class UserServiceTest {

    private final IPropertyService propertyService = new PropertyService();

    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private final IAdminUserService adminUserService = new AdminUserService(connectionService, propertyService);

    private final IUserService userService = new UserService(connectionService, propertyService);

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final UserDTO user = new UserDTO();
        Assert.assertNotNull(adminUserService.add(user));
    }

}
