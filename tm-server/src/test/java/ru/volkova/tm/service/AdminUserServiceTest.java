package ru.volkova.tm.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.service.IAdminUserService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.dto.UserDTO;
import ru.volkova.tm.marker.UnitCategory;

import java.util.ArrayList;
import java.util.List;

public class AdminUserServiceTest {

    private final IPropertyService propertyService = new PropertyService();

    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private final IAdminUserService adminUserService = new AdminUserService(connectionService, propertyService);

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final UserDTO user = new UserDTO();
        Assert.assertNotNull(adminUserService.add(user));
    }

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<UserDTO> userList = new ArrayList<>();
        final UserDTO user1 = new UserDTO();
        final UserDTO user2 = new UserDTO();
        userList.add(user1);
        userList.add(user2);
        adminUserService.addAll(userList);
        Assert.assertEquals(adminUserService.findById(user1.getId()),
                user1);
        Assert.assertEquals(adminUserService.findById(user2.getId()),
                user2);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final UserDTO user = new UserDTO();
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findAll());
        adminUserService.removeById(user.getId());
        Assert.assertTrue(adminUserService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        final UserDTO user = new UserDTO();
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findAll());
        adminUserService.clear();
        Assert.assertTrue(adminUserService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findById() {
        final UserDTO user = new UserDTO();
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findById(user.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByEmail() {
        final UserDTO user = new UserDTO();
        user.setEmail("lala@la.ru");
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findByEmail(user.getEmail()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByLogin() {
        final UserDTO user = new UserDTO();
        user.setLogin("test");
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findByLogin(user.getLogin()));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeById() {
        final UserDTO user = new UserDTO();
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findById(user.getId()));
        adminUserService.removeById(user.getId());
        Assert.assertNotNull(adminUserService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByEmail() {
        final UserDTO user = new UserDTO();
        user.setEmail("lala@la.ru");
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findByEmail(user.getEmail()));
        adminUserService.removeByEmail(user.getEmail());
        Assert.assertNotNull(adminUserService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByLogin() {
        final UserDTO user = new UserDTO();
        user.setLogin("test");
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findByLogin(user.getLogin()));
        adminUserService.removeByLogin(user.getLogin());
        Assert.assertNotNull(adminUserService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void lockById() {
        final UserDTO user= new UserDTO();
        adminUserService.add(user);
        adminUserService.lockById(user.getId());
        Assert.assertEquals(false, user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void lockByEmail() {
        final UserDTO user= new UserDTO();
        user.setEmail("test@test.ru");
        adminUserService.add(user);
        adminUserService.lockByEmail(user.getEmail());
        Assert.assertEquals(false, user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void lockByLogin() {
        final UserDTO user= new UserDTO();
        user.setLogin("test");
        adminUserService.add(user);
        adminUserService.lockByLogin(user.getLogin());
        Assert.assertEquals(false, user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void unlockById() {
        final UserDTO user= new UserDTO();
        adminUserService.add(user);
        adminUserService.lockById(user.getId());
        Assert.assertEquals(false, user.getLocked());
        adminUserService.unlockById(user.getId());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void unlockByEmail() {
        final UserDTO user= new UserDTO();
        user.setEmail("test@test.ru");
        adminUserService.add(user);
        adminUserService.lockByEmail(user.getEmail());
        Assert.assertEquals(false, user.getLocked());
        adminUserService.unlockByEmail(user.getEmail());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void unlockByLogin() {
        final UserDTO user= new UserDTO();
        user.setLogin("test");
        adminUserService.add(user);
        adminUserService.lockByLogin(user.getLogin());
        Assert.assertEquals(false, user.getLocked());
        adminUserService.unlockByLogin(user.getLogin());
        Assert.assertTrue(user.getLocked());
    }

}
