package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.IProjectTaskService;
import ru.volkova.tm.api.service.ITaskService;
import ru.volkova.tm.dto.ProjectDTO;
import ru.volkova.tm.dto.TaskDTO;
import ru.volkova.tm.dto.UserDTO;
import ru.volkova.tm.marker.UnitCategory;

import java.util.List;

public class ProjectTaskServiceTest {

    private final IPropertyService propertyService = new PropertyService();

    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    private final UserDTO user = new UserDTO();

    @Test
    @Category(UnitCategory.class)
    public void bindTaskByProjectIdTest() {
        final ProjectDTO project = new ProjectDTO();
        final TaskDTO task = new TaskDTO();
        task.setUserId(user.getId());
        taskService.insert(task);
        projectTaskService.bindTaskByProjectId(task.getUserId(), project.getId(), task.getId());
        Assert.assertNotNull(task.getProjectId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllByProjectIdTest() {
        final ProjectDTO project = new ProjectDTO();
        project.setUserId(user.getId());
        final TaskDTO task1 = new TaskDTO();
        final TaskDTO task2 = new TaskDTO();
        task1.setUserId(user.getId());
        task2.setUserId(user.getId());
        taskService.insert(task1);
        taskService.insert(task2);
        projectTaskService.bindTaskByProjectId(user.getId(), project.getId(), task1.getId());
        projectTaskService.bindTaskByProjectId(user.getId(), project.getId(), task2.getId());
        List<TaskDTO> taskList = projectTaskService.findAllTasksByProjectId(project.getUserId(), project.getId());
        Assert.assertNotNull(taskList.contains(task1));
        Assert.assertTrue(taskList.contains(task2));
    }

    @Test
    @Category(UnitCategory.class)
    public void unbindTaskByProjectIdTest() {
        final ProjectDTO project = new ProjectDTO();
        final TaskDTO task = new TaskDTO();
        task.setUserId(user.getId());
        taskService.insert(task);
        projectTaskService.bindTaskByProjectId(task.getUserId(), project.getId(), task.getId());
        projectTaskService.unbindTaskByProjectId(task.getUserId(), project.getId(), task.getId());
        Assert.assertNull(task.getProjectId());
    }

}
