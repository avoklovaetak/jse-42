package ru.volkova.tm.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.IProjectService;
import ru.volkova.tm.dto.ProjectDTO;
import ru.volkova.tm.dto.UserDTO;
import ru.volkova.tm.marker.UnitCategory;

import static ru.volkova.tm.enumerated.Status.*;

public class ProjectServiceTest {

    private final IPropertyService propertyService = new PropertyService();

    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private final IProjectService projectService = new ProjectService(connectionService);

    private final UserDTO user  = new UserDTO();

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        projectService.add(user.getId(), "DEMO", "project");
        final ProjectDTO project = projectService.findOneByName(user.getId(), "DEMO");
        Assert.assertNotNull(project);
    }

    @Test
    @Category(UnitCategory.class)
    public void changeOneStatusByIdTest() {
        final ProjectDTO project = new ProjectDTO();
        project.setUserId(user.getId());
        projectService.insert(project);
        projectService.changeOneStatusById(user.getId(), project.getId(), NOT_STARTED);
        Assert.assertEquals(NOT_STARTED, project.getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeOneStatusByNameTest() {
        final ProjectDTO project = new ProjectDTO();
        project.setUserId(user.getId());
        project.setName("test");
        projectService.insert(project);
        projectService.changeOneStatusByName(user.getId(), project.getName(), IN_PROGRESS);
        Assert.assertEquals(IN_PROGRESS, project.getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        final ProjectDTO project = new ProjectDTO();
        project.setUserId(user.getId());
        projectService.insert(project);
        projectService.clear(project.getUserId());
        Assert.assertTrue(projectService.findAll(user.getId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIdTest() {
        final ProjectDTO project = new ProjectDTO();
        project.setUserId(user.getId());
        projectService.insert(project);
        Assert.assertNotNull(projectService.findById(project.getUserId(), project.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTest() {
        final ProjectDTO project = new ProjectDTO();
        project.setUserId(user.getId());
        projectService.insert(project);
        Assert.assertNotNull(projectService.findOneByIndex(project.getUserId(), 0));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByNameTest() {
        final ProjectDTO project = new ProjectDTO();
        project.setUserId(user.getId());
        project.setName("DEMO");
        projectService.insert(project);
        Assert.assertNotNull(projectService.findOneByName(project.getUserId(), project.getName()));
    }

}
