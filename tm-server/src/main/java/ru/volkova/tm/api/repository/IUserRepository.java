package ru.volkova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.dto.UserDTO;

import java.util.List;

public interface IUserRepository extends IRepository<UserDTO> {

    @Delete("DELETE FROM 'user'")
    void clear();

    @Select("SELECT * FROM 'user'")
    @Result(column = "id", property = "id")
    @Result(column = "email", property = "email")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "secondName")
    @Result(column = "login", property = "login")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "role", property = "role")
    @Result(column = "locked", property = "locked")
    @NotNull
    List<UserDTO> findAll();

    @Select("SELECT * FROM 'user' WHERE email = #{email} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "email", property = "email")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "secondName")
    @Result(column = "login", property = "login")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "role", property = "role")
    @Result(column = "locked", property = "locked")
    @Nullable
    UserDTO findByEmail(@NotNull String email);

    @Select("SELECT * FROM 'user' WHERE id = #{id} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "email", property = "email")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "secondName")
    @Result(column = "login", property = "login")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "role", property = "role")
    @Result(column = "locked", property = "locked")
    @Nullable
    UserDTO findById(@NotNull String id);

    @Select("SELECT * FROM 'user' WHERE login = #{login} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "email", property = "email")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "secondName")
    @Result(column = "login", property = "login")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "role", property = "role")
    @Result(column = "locked", property = "locked")
    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Update("UPDATE TABLE 'user' SET locked = #{locked} WHERE email = #{email} LIMIT 1")
    void lockByEmail(@NotNull String email);

    @Update("UPDATE TABLE 'user' SET locked = #{locked} WHERE id = #{id} LIMIT 1")
    void lockById(@NotNull String id);

    @Update("UPDATE TABLE 'user' SET locked = #{locked} WHERE login = #{login} LIMIT 1")
    void lockByLogin(@NotNull String login);

    @Delete("DELETE FROM 'user' WHERE email = #{email}")
    void removeByEmail(@NotNull String email);

    @Delete("DELETE FROM 'user' WHERE id = #{id}")
    void removeById(@NotNull String id);

    @Delete("DELETE FROM 'user' WHERE login = #{login}")
    void removeByLogin(@NotNull String login);

    @Update("UPDATE TABLE 'user' SET locked = #{locked} WHERE email = #{email} LIMIT 1")
    void unlockByEmail(@NotNull String email);

    @Update("UPDATE TABLE 'user' SET locked = #{locked} WHERE id = #{id} LIMIT 1")
    void unlockById(@NotNull String id);

    @Update("UPDATE TABLE 'user' SET password_hash = #{password_hash} WHERE user_id = #{userId} LIMIT 1")
    void setPassword(
            @NotNull final String userId,
            @Nullable final String hash
    );

    @Update("UPDATE TABLE 'user' SET locked = #{locked} WHERE login = #{login} LIMIT 1")
    void unlockByLogin(@NotNull String login);

    @Update("UPDATE TABLE 'user' SET" +
            " first_name = #{first_name}, second_name = #{second_name}," +
            " middle_name = {middle_name} WHERE user_id = ? LIMIT 1")
    void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String secondName,
            @Nullable final String middleName
    );

    @Insert("INSERT INTO 'user'('id','email','first_name'," +
            "'last_name','middle_name','role','locked','login','password_hash') " +
            "VALUES(#{id},#{email},#{firstName},#{secondName},#{middleName}," +
            "#{role},#{locked},#{login},#{passwordHash})")
    @Nullable
    UserDTO add(@Nullable final UserDTO user);

}
