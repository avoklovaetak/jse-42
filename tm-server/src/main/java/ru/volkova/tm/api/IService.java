package ru.volkova.tm.api;

import ru.volkova.tm.dto.AbstractEntityDTO;

public interface IService<E extends AbstractEntityDTO> extends IRepository<E> {

}
