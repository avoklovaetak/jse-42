package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IOwnerRepository;
import ru.volkova.tm.dto.AbstractOwnerEntityDTO;
import ru.volkova.tm.enumerated.Status;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntityDTO> extends IOwnerRepository<E> {

    @Nullable
    E changeOneStatusById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @Nullable
    E changeOneStatusByIndex(
            @NotNull String userId,
            @NotNull Integer index,
            @Nullable Status status
    );

    @Nullable
    E changeOneStatusByName(
            @NotNull String userId,
            @Nullable String name,
            @Nullable Status status
    );

    @NotNull
    List<E> findAll(@NotNull String userId, @Nullable Comparator<E> comparator);

    @Nullable
    E findOneByIndex(@NotNull String userId, @Nullable Integer index);

    @Nullable
    E findOneByName(@NotNull String userId, @Nullable String name);

    @Nullable
    E finishOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    E finishOneByIndex(@NotNull String userId, @Nullable Integer index);

    @Nullable
    E finishOneByName(@NotNull String userId, @Nullable String name);

    void removeOneByIndex(@NotNull String userId, @Nullable Integer index);

    void removeOneByName(@NotNull String userId, @Nullable String name);

    @Nullable
    E startOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    E startOneByIndex(@NotNull String userId, @Nullable Integer index);

    @Nullable
    E startOneByName(@NotNull String userId, @Nullable String name);

    @Nullable
    E updateOneById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description);

    @Nullable
    E updateOneByIndex(
            @NotNull String userId,
            @NotNull Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
