package ru.volkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.SessionDTO;
import ru.volkova.tm.dto.TaskDTO;
import ru.volkova.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    TaskDTO addTask(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @NotNull @WebParam(name = "entity", partName = "entity") TaskDTO entity
    );

    @WebMethod
    void clearTasks(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @Nullable
    @WebMethod
    List<TaskDTO> findAllTasks(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @Nullable
    @WebMethod
    TaskDTO findTaskById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    void removeTaskById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    );

    @Nullable
    @WebMethod
    TaskDTO changeTaskStatusById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "status", partName = "id") Status status
    );

    @Nullable
    @WebMethod
    TaskDTO changeTaskStatusByName(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    );

    @Nullable
    @WebMethod
    TaskDTO findTaskByIndex(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @Nullable
    @WebMethod
    TaskDTO findTaskByName(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    void removeTaskByName(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @Nullable
    @WebMethod
    TaskDTO updateTaskById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );


    @WebMethod
    void addTaskByUser(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

}
