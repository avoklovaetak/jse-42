package ru.volkova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.dto.ProjectDTO;
import ru.volkova.tm.enumerated.Status;

import java.util.List;

public interface IProjectRepository extends IRepository<ProjectDTO> {

    @Insert("INSERT INTO 'project' ('id','name','description,'" +
            "'project_id','date_start','date_end','created','status','user_id'" +
            "VALUES (#{id},#{name},#{description},#{projectId}," +
            "#{dateStart},#{dateFinish},#{created},#{status},#{userId})")
    @Nullable
    ProjectDTO insert(@Nullable final ProjectDTO project);

    @Delete("DELETE FROM 'project'")
    void clear(@NotNull String userId);

    @Select("SELECT * FROM 'project' WHERE user_id = #{userId}")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "project_id", property = "projectId")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_end", property = "dateFinish")
    @Result(column = "created", property = "created")
    @Result(column = "status", property = "status")
    @Result(column = "user_id", property = "userId")
    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId);

    @Select("SELECT * FROM 'project' WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_end", property = "dateFinish")
    @Result(column = "created", property = "created")
    @Result(column = "status", property = "status")
    @Result(column = "user_id", property = "userId")
    @Nullable
    ProjectDTO findById(@NotNull final String userId, @NotNull final String id);

    @Select("SELECT * FROM 'project' WHERE user_id = #{userId} LIMIT #{index},1")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_end", property = "dateFinish")
    @Result(column = "created", property = "created")
    @Result(column = "status", property = "status")
    @Result(column = "user_id", property = "userId")
    @Nullable
    ProjectDTO findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Select("SELECT * FROM 'project' WHERE name = #{name} AND user_id = #{userId} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_end", property = "dateFinish")
    @Result(column = "created", property = "created")
    @Result(column = "status", property = "status")
    @Result(column = "user_id", property = "userId")
    @Nullable
    ProjectDTO findOneByName(@NotNull String userId, @NotNull String name);

    @Update("UPDATE TABLE 'project' SET status = #{status} WHERE id = #{id} AND user_id = #{userId}")
    @Nullable
    ProjectDTO changeOneStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    );

    @Update("UPDATE TABLE 'project' SET status = #{status} WHERE name = #{name} AND user_id = #{userId}")
    @Nullable
    ProjectDTO changeOneStatusByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final Status status
    );

    @Delete("DELETE FROM 'project' WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    void removeById(@NotNull String userId,@NotNull String id);

    @Delete("DELETE FROM 'project' WHERE name = #{name} AND user_id = #{userId} LIMIT 1")
    void removeOneByName(@NotNull String userId,@Nullable String name);

    @Update("UPDATE TABLE 'project' SET name = #{name}, description = #{description}"
            + " WHERE id = #{id} AND user_id = #{userId}")
    @Nullable
    ProjectDTO updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
