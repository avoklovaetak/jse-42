package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IService;
import ru.volkova.tm.dto.SessionDTO;
import ru.volkova.tm.enumerated.Role;
import java.util.List;

public interface ISessionService extends IService<SessionDTO> {

    void add(@NotNull SessionDTO session);

    @Nullable
    SessionDTO open(String login, String password);

    @Nullable
    SessionDTO close(@Nullable SessionDTO session);

    @Nullable
    List<SessionDTO> findAll();

    boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    );

    void validate(@Nullable final SessionDTO session);

    void validateAdmin(@Nullable final SessionDTO session, @Nullable final Role role);

}
