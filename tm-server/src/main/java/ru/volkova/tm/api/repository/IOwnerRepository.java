package ru.volkova.tm.api.repository;

import java.util.Comparator;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.dto.AbstractOwnerEntityDTO;
import ru.volkova.tm.enumerated.Status;

public interface IOwnerRepository<E extends AbstractOwnerEntityDTO> extends IRepository<E> {

    void clear(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @Nullable
    E findById(@NotNull String userId,@NotNull String id);

    @Nullable
    E findOneByIndex(@NotNull String userId,@NotNull Integer index);

    @Nullable
    E findOneByName(@NotNull String userId,@NotNull String name);

    @Nullable
    E finishOneById(@NotNull final String userId, @Nullable final String id);

    @Nullable
    E finishOneByName(@NotNull final String userId, @Nullable final String name);

    @Nullable
    E finishOneByIndex(@NotNull final String userId, @NotNull final Integer index);

    @Nullable
    E startOneById(@NotNull final String userId, @Nullable final String id);

    @Nullable
    E startOneByName(@NotNull final String userId, @Nullable final String name);

    @Nullable
    E startOneByIndex(@NotNull final String userId, @NotNull final Integer index);

    @Nullable
    E changeOneStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    );

    @Nullable
    E changeOneStatusByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final Status status
    );

    @Nullable
    E changeOneStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @Nullable final Status status
    );

    void removeById(@NotNull String userId,@NotNull String id);

    void removeOneByIndex(@NotNull String userId,@NotNull Integer index);

    void removeOneByName(@NotNull String userId,@Nullable String name);

    @Nullable
    E updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

    @Nullable
    E updateOneByIndex(
            @NotNull String userId,
            @NotNull Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
