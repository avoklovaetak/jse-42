package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IService;
import ru.volkova.tm.dto.UserDTO;
import ru.volkova.tm.enumerated.Role;


import java.util.List;

public interface IAdminUserService extends IService<UserDTO> {

    void clear();

    @NotNull
    UserDTO add(@Nullable final UserDTO user);

    void addAll(@Nullable List<UserDTO> entities);

    @NotNull
    UserDTO createUser(@Nullable final String login, @Nullable final String password);

    @NotNull
    UserDTO createUserWithEmail(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    );

    @NotNull
    UserDTO createUserWithRole
            (@Nullable final String login,
             @Nullable final String password,
             @Nullable final Role role);

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findByEmail(@Nullable final String email);

    @Nullable
    UserDTO findById(@Nullable final String id);

    @Nullable
    UserDTO findByLogin(@Nullable final String login);

    boolean isEmailExists(@Nullable final String email);

    boolean isLoginExists(@Nullable final String login);

    void lockByEmail(@Nullable String email);

    void lockById(@Nullable String id);

    void lockByLogin(@Nullable String login);

    void removeByEmail(@Nullable String email);

    void removeById(@Nullable final String id);

    void removeByLogin(@Nullable final String login);

    void unlockByEmail(@Nullable String email);

    void unlockById(@Nullable String id);

    void unlockByLogin(@Nullable String login);

    void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String secondName,
            @Nullable final String middleName
    );

}
