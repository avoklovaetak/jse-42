package ru.volkova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.dto.SessionDTO;

public interface ISessionRepository extends IRepository<SessionDTO> {

    @Insert("INSERT INTO `session`(`id`, `user_id`, `signature`, `timestamp`) " +
            "VALUES(#{id}, #{userId}, #{signature}, #{timestamp})")
    void add(@NotNull SessionDTO session);

    @Delete("DELETE FROM `session`")
    void close(@NotNull SessionDTO session);

}
