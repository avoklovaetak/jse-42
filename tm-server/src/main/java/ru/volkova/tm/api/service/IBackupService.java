package ru.volkova.tm.api.service;

import lombok.SneakyThrows;
import ru.volkova.tm.dto.DomainDTO;

public interface IBackupService {

    @SneakyThrows
    void load();

    @SneakyThrows
    void save();

    @SneakyThrows
    public DomainDTO getDomain();

    @SneakyThrows
    public void setDomain(final DomainDTO domain);

}
