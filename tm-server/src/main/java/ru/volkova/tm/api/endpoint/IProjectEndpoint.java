package ru.volkova.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.ProjectDTO;
import ru.volkova.tm.dto.SessionDTO;
import ru.volkova.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @SneakyThrows
    @Nullable
    @WebMethod
    ProjectDTO changeProjectOneStatusById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    );

    @SneakyThrows
    @Nullable
    @WebMethod
    ProjectDTO changeProjectStatusByName(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    );

    @WebMethod
    public void clearProject(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @SneakyThrows
    @NotNull
    @WebMethod
    List<ProjectDTO> findAllProjects(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @Nullable
    @WebMethod
    ProjectDTO findProjectById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    );

    @Nullable
    @WebMethod
    ProjectDTO findProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @Nullable
    @WebMethod
    ProjectDTO findProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    void removeProjectById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    void removeProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @Nullable
    @WebMethod
    ProjectDTO updateProjectById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @WebMethod
    void addProjectByUser(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

}
