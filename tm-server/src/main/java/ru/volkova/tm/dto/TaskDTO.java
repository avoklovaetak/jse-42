package ru.volkova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.entity.IWBS;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "task")
public class TaskDTO extends AbstractOwnerEntityDTO implements IWBS {

    @Column(name = "project_id")
    @Nullable
    private String projectId;

    @NotNull
    public String toString() {
        return id + ": " + getName() + ": " + projectId
                + "; " + "created: " + getCreated() +
                "started: " + getDateStart();
    }

}
