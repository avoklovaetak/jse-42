package ru.volkova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public class AbstractOwnerEntityDTO extends AbstractEntityDTO {

    @Column(name = "user_id")
    @Nullable
    protected String userId;

    @Column
    @NotNull
    private String name = "";

    @Column
    @Nullable
    private String description = "";

    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status = Status.NOT_STARTED;

    @Column(name = "date_start")
    @Nullable
    private Date dateStart;

    @Column(name = "date_end")
    @Nullable
    private Date dateFinish;

    @Column(name = "created")
    @Nullable
    private Date created = new Date();

}
