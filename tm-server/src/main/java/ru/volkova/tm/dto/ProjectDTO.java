package ru.volkova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.entity.IWBS;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "project")
public class ProjectDTO extends AbstractOwnerEntityDTO implements IWBS {

    @NotNull
    public String toString() {
        return id + ": " + getName() + "; " +
                "created: " + getCreated() +
                "started: " + getDateStart();
    }

}
