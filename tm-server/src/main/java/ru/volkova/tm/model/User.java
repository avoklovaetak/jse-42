package ru.volkova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "user")
public class User extends AbstractEntity {

    @Column
    @Nullable
    private String login;

    @Column(name = "password_hash")
    @Nullable
    private String passwordHash;

    @Column
    @Nullable
    private String email;

    @Column(name = "middle_name")
    @Nullable
    private String middleName;

    @Column(name = "first_name")
    @Nullable
    private String firstName;

    @Column(name = "last_name")
    @Nullable
    private String secondName;

    @Column
    @NotNull
    private Boolean locked = true;

    @Enumerated(EnumType.STRING)
    @Nullable
    private Role role = Role.USER;

    @Nullable
    @OneToMany(mappedBy = "user")
    private List<Task> tasks = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user")
    private List<Project> projects = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user")
    private List<Session> sessions = new ArrayList<>();

}
