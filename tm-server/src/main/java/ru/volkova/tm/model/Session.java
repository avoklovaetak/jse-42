package ru.volkova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "session")
public class Session extends AbstractEntity implements Cloneable {

    @Column
    @Nullable
    Long timestamp;

    @NotNull
    @ManyToOne
    User userId;

    @Column
    @Nullable
    String signature;

}
