package ru.volkova.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.endpoint.IAdminEndpoint;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.dto.SessionDTO;
import ru.volkova.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public void dataBinarySave(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
            ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataBinarySave();
    }

    @WebMethod
    public void dataBinaryLoad(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataBinaryLoad();
    }

    @WebMethod
    public void dataJsonLoad(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataJsonLoad();
    }

    @WebMethod
    public void dataJsonSave(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataJsonSave();
    }

    @WebMethod
    public void dataBase64Load(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataBase64Load();
    }

    @WebMethod
    public void dataBase64Save(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataBase64Save();
    }

    @WebMethod
    public void dataJsonLoadFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataJsonLoadFasterxml();
    }

    @WebMethod
    public void dataJsonSaveFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataJsonSaveFasterxml();
    }

    @WebMethod
    public void dataJsonLoadJaxb(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataJsonLoadJaxb();
    }

    @WebMethod
    public void dataJsonSaveJaxb(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataJsonSaveJaxb();
    }

    @WebMethod
    public void dataXmlLoadFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataXmlLoadFasterxml();
    }

    @WebMethod
    public void dataXmlSaveFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataXmlSaveFasterxml();
    }

    @WebMethod
    public void dataXmlLoadJaxb(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataXmlLoadJaxb();
    }

    @WebMethod
    public void dataXmlSaveJaxb(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataXmlSaveJaxb();
    }

    @WebMethod
    public void dataYamlLoadFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataYamlLoadFasterxml();
    }

    @WebMethod
    public void dataYamlSaveFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminService().DataYamlSaveFasterxml();
    }

}
