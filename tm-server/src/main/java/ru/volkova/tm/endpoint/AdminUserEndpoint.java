package ru.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.endpoint.IAdminUserEndpoint;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.dto.SessionDTO;
import ru.volkova.tm.dto.UserDTO;
import ru.volkova.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    public AdminUserEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @WebMethod
    public UserDTO addUser(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session,
            @NotNull @WebParam(name = "entity", partName = "entity") UserDTO entity
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getAdminUserService().add(entity);
    }

    @WebMethod
    public void clearUsers(@NotNull @WebParam(name = "session", partName = "session") SessionDTO session) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().clear();
    }

    @NotNull
    @WebMethod
    public UserDTO createUserByLogPass(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getAdminUserService().createUser(login, password);
    }

    @NotNull
    @WebMethod
    public UserDTO createUserWithEmail(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getAdminUserService().createUserWithEmail(login, password, email);
    }

    @NotNull
    @WebMethod
    public UserDTO createUserWithRole(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "role", partName = "role") Role role
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getAdminUserService().createUserWithRole(login, password, role);
    }

    @NotNull
    @WebMethod
    public List<UserDTO> findAllUser(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getAdminUserService().findAll();
    }

    @Nullable
    @WebMethod
    public UserDTO findUserById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getAdminUserService().findById(id);
    }

    @Nullable
    @WebMethod
    public UserDTO findUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getAdminUserService().findByLogin(login);
    }

    @WebMethod
    public boolean isEmailExists(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getAdminUserService().isEmailExists(email);
    }

    @WebMethod
    public boolean isLoginExists(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getAdminUserService().isLoginExists(login);
    }

    @WebMethod
    public void lockByEmail(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().lockByEmail(email);
    }

    @WebMethod
    public void lockById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().lockById(id);
    }

    @WebMethod
    public void lockByLogin(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().lockByLogin(login);
    }

    @WebMethod
    public void removeUserByEmail(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().removeByEmail(email);
    }

    @WebMethod
    public void removeUserById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().removeById(id);
    }

    @WebMethod
    public void removeUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().removeByLogin(login);
    }


    @WebMethod
    public void unlockUserByEmail(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().unlockByEmail(email);
    }

    @WebMethod
    public void unlockUserById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().unlockById(id);
    }

    @WebMethod
    public void unlockUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getAdminUserService().unlockByLogin(login);
    }

    @WebMethod
    public void updateUser(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name = "secondName", partName = "secondName") String secondName,
            @Nullable @WebParam(name = "middleName", partName = "middleName") String middleName
    ) {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        final String userId = session.getUserId();
        serviceLocator.getAdminUserService()
                .updateUser(userId, firstName, secondName, middleName);
    }

}
