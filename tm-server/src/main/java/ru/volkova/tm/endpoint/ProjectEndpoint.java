package ru.volkova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.endpoint.IProjectEndpoint;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.dto.ProjectDTO;
import ru.volkova.tm.dto.SessionDTO;
import ru.volkova.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @SneakyThrows
    @Nullable
    @WebMethod
    public ProjectDTO changeProjectOneStatusById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService()
                .changeOneStatusById(session.getUserId(), id, status);
    }

    @SneakyThrows
    @Nullable
    @WebMethod
    public ProjectDTO changeProjectStatusByName(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService()
                .changeOneStatusByName(session.getUserId(), name, status);
    }

    @WebMethod
    public void clearProject(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        serviceLocator.getProjectService().clear(userId);
    }

    @SneakyThrows
    @NotNull
    @WebMethod
    public List<ProjectDTO> findAllProjects(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getProjectService().findAll(userId);
    }

    @Nullable
    @WebMethod
    public ProjectDTO findProjectById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getProjectService().findById(userId, id);
    }

    @Nullable
    @WebMethod
    public ProjectDTO findProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getProjectService().findOneByIndex(userId, index);
    }

    @Nullable
    @WebMethod
    public ProjectDTO findProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getProjectService().findOneByName(userId, name);
    }

    @WebMethod
    public void removeProjectById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        serviceLocator.getProjectService().removeById(userId, id);
    }

    @WebMethod
    public void removeProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        serviceLocator.getProjectService().removeOneByName(userId, name);
    }

    @Nullable
    @WebMethod
    public ProjectDTO updateProjectById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getProjectService()
                .updateOneById(userId, id, name, description);
    }

    @WebMethod
    public void addProjectByUser(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        serviceLocator.getProjectService().add(userId, name, description);
    }

}
