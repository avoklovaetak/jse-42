package ru.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.endpoint.IUserEndpoint;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public void setPassword(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "password", partName = "password") String password
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        serviceLocator.getUserService().setPassword(userId, password);
    }

}
