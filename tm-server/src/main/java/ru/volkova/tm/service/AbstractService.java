package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.IService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.dto.AbstractEntityDTO;

public abstract class AbstractService<E extends AbstractEntityDTO> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
