package ru.volkova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.ISessionRepository;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.ISessionService;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.dto.SessionDTO;
import ru.volkova.tm.dto.UserDTO;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.util.HashUtil;
import ru.volkova.tm.api.IPropertyService;
import java.util.List;

public final class SessionService extends AbstractService<SessionDTO> implements ISessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionService(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final IConnectionService connectionService) {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    public void add(@NotNull SessionDTO session) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.add(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @Nullable final UserDTO user = serviceLocator.getAdminUserService().findByLogin(login);
        if (user == null) return null;
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        @Nullable final SessionDTO signSession = sign(session);
        if (signSession == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.add(signSession);
            sqlSession.commit();
            return signSession;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final UserDTO user = serviceLocator.getAdminUserService().findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        final String passwordHash2 = user.getPasswordHash();
        return passwordHash.equals(passwordHash2);
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        final String signature = session.getSignature();
        if (signature == null || signature.isEmpty()) throw new AccessDeniedException();
        final String userId = session.getUserId();
        if (userId.isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final SessionDTO sessionTarget = sign(temp);
        if (sessionTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validateAdmin(@Nullable final SessionDTO session, @Nullable final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        if (session.getUserId().isEmpty()) throw new AccessDeniedException();
        validate(session);
        final @Nullable UserDTO user = serviceLocator.getAdminUserService().findById(session.getUserId());
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

    @Override
    @Nullable
    public SessionDTO close(@Nullable final SessionDTO session) {
        if (session == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.close(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return session;
    }

    @Override
    @Nullable
    public List<SessionDTO> findAll() {
        return null;
    }

    @Nullable
    public SessionDTO sign(@Nullable final SessionDTO session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @Nullable final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

}
