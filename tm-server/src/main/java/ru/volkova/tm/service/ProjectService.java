package ru.volkova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IProjectRepository;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.IProjectService;
import ru.volkova.tm.dto.ProjectDTO;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;

import java.util.List;

public final class ProjectService extends AbstractService<ProjectDTO> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @SneakyThrows
    public void insert(@Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.insert(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void add(@NotNull String userId, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new ObjectNotFoundException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        insert(project);
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<ProjectDTO> entities) {
        if (entities == null) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            entities.forEach(projectRepository::insert);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.clear(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@NotNull String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            final List<ProjectDTO> projects = projectRepository.findAll(userId);
            sqlSession.commit();
            return projects;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO findById(@NotNull String userId, @NotNull String id) {
        if (id.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            final ProjectDTO project = projectRepository.findById(userId,id);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndex(@NotNull String userId, @NotNull Integer index) {
        if (index < 0) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            final ProjectDTO project = projectRepository.findOneByIndex(userId,index);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO findOneByName(@NotNull String userId, @NotNull String name) {
        if (name.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            final ProjectDTO project = projectRepository.findOneByName(userId,name);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO changeOneStatusById(@NotNull String userId, @NotNull String id, @Nullable Status status) {
        if (id.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            final ProjectDTO project = projectRepository.changeOneStatusById(userId, id, status);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO changeOneStatusByName(@NotNull String userId, @NotNull String name, @Nullable Status status) {
        if (name.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            final ProjectDTO project = projectRepository.changeOneStatusByName(userId, name, status);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        if (id.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeById(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeOneByName(@NotNull String userId, @NotNull String name) {
        if (name.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeOneByName(userId, name);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public @Nullable ProjectDTO updateOneById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            final ProjectDTO project = projectRepository.updateOneById(userId, id, name, description);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }
}
