package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.service.IAdminUserService;
import ru.volkova.tm.api.service.IAuthService;
import ru.volkova.tm.dto.UserDTO;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.empty.EmptyLoginException;
import ru.volkova.tm.exception.empty.EmptyPasswordException;
import ru.volkova.tm.exception.entity.UserNotFoundException;
import ru.volkova.tm.util.HashUtil;

public class AuthService implements IAuthService {

    @NotNull
    private final IAdminUserService adminUserService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    public AuthService(
            @NotNull final IAdminUserService adminUserService,
            @NotNull final IPropertyService propertyService
    ) {
        this.adminUserService = adminUserService;
        this.propertyService = propertyService;
    }

    @Override
    public void checkRoles(@Nullable Role... roles) {
        if (roles == null || roles.length == 0) return;
        @NotNull final UserDTO user = getUser();
        if (user == null) throw new AccessDeniedException();
        @Nullable final Boolean locked = user.getLocked();
        if (!locked) throw new AccessDeniedException();
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) {
            if (role.equals(item)) return;
        }
        throw new AccessDeniedException();
    }

    @Nullable
    @Override
    public UserDTO getUser() {
        @NotNull final String userId = getUserId();
        return adminUserService.findById(userId);
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    public void login(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final UserDTO user = adminUserService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        final String hash = HashUtil.salt(propertyService, password);
        if (hash == null || hash.isEmpty()) return;
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    public void registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        adminUserService.createUserWithEmail(login, password, email);
    }

}
