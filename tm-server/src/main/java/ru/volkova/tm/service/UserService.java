package ru.volkova.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.IUserService;
import ru.volkova.tm.dto.UserDTO;
import ru.volkova.tm.exception.empty.*;
import ru.volkova.tm.exception.entity.UserNotFoundException;
import ru.volkova.tm.util.HashUtil;

public final class UserService extends AbstractService<UserDTO> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    public void setPassword(
            @NotNull final String userId,
            @Nullable final String password
    ) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            if (password == null || password.isEmpty()) throw new EmptyPasswordException();
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            final UserDTO user = userRepository.findById(userId);
            sqlSession.commit();
            if (user == null) throw new UserNotFoundException();
            final String hash = HashUtil.salt(propertyService, password);
            userRepository.setPassword(userId, hash);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
