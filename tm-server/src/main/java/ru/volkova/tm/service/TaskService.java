package ru.volkova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.ITaskService;
import ru.volkova.tm.dto.TaskDTO;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;

import java.util.List;

public final class TaskService extends AbstractService<TaskDTO> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public TaskDTO insert(@NotNull final TaskDTO task) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.insert(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void add(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new ObjectNotFoundException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.insert(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<TaskDTO> entities) {
        if (entities == null) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            entities.forEach(taskRepository::insert);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.clear(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    public List<TaskDTO> findAll(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            final List<TaskDTO> tasks = taskRepository.findAll(userId);
            sqlSession.commit();
            return tasks;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    public TaskDTO findById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            final TaskDTO task = taskRepository.findById(userId,id);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    public TaskDTO findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (index < 0) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            final TaskDTO task = taskRepository.findOneByIndex(userId,index);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO findOneByName(@NotNull String userId, @NotNull String name) {
        if (name.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            final TaskDTO task = taskRepository.findOneByName(userId,name);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO changeOneStatusById(
            @NotNull String userId, @NotNull String id, @Nullable Status status
    ) {
        if (id.isEmpty()) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            final TaskDTO task = taskRepository.changeOneStatusById(userId, id, status);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO changeOneStatusByName(
            @NotNull String userId, @NotNull String name, @Nullable Status status
    ) {
        if (name.isEmpty()) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            final TaskDTO task = taskRepository.changeOneStatusByName(userId, name, status);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }


    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        if (id.isEmpty()) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeById(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeOneByName(@NotNull String userId, @NotNull String name) {
        if (name.isEmpty()) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeById(userId, name);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO updateOneById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            final TaskDTO task = taskRepository.updateOneById(userId, id, name, description);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
